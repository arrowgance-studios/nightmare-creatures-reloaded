## Nightmare Creatures Reloaded

[Google docs](https://docs.google.com/document/d/1OVHEGYf672kLrIyPdm-mBPSi4VneZiVHuDXXsv1yx80/edit#)

## Build instructions

### Windows
```bash
# make sure to clone with submodules
git clone --recursive git@gitlab.com:arrowgance-studios/nightmare-creatures-reloaded.git
# or populate submodules later with:
git submodule update --init --recursive

# create build directory
mkdir bin && cd bin

# invoke Cmake
cmake ..

# build the executable
cmake --build .
```

It is also possible to use the `NCR.sln` file from `win/` directory.

### Debian/Ubuntu

```bash
# install dependencies
sudo apt install build-essential cmake pkg-config libglew-dev libglfw3-dev libglm-dev libxrandr-dev libxi-dev

# create build directory
mkdir bin && cd bin

# invoke Cmake
cmake ..

# build the executable
make
```
