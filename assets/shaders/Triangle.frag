#version 330

in vec2 theTextureCoords;
out vec4 outputColor;
uniform sampler2D diffuseTexture;

void main()
{
   outputColor = texture(diffuseTexture, theTextureCoords);
}
