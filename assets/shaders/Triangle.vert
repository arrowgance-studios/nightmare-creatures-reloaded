#version 330

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 textureCoords;
out vec2 theTextureCoords;

void main()
{
   gl_Position = position;
   theTextureCoords = textureCoords;
}