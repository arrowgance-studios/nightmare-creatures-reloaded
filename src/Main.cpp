#include "Game.h"

int main()
{
	Game game;
	game.Init();
	game.Loop();
	game.Terminate();
	return 0;
}