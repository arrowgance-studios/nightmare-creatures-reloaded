#include "Game.h"
#include <GLFW/glfw3.h>
#include <cstdio>
#include <cstdlib>

static void GlfwErrorCallback(int error, const char* description)
{
	printf("Error: %s\n", description);
}

static void APIENTRY KhrErrorCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
	const char* severityStrings[] = {
		"severity: HIGH",
		"severity: MEDIUM",
		"severity: LOW",
		"severity: NOTIFICATION"
	};

	const char* sourceStrings[] = {
		"source: API",
		"source: WINDOW SYSTEM",
		"source: SHADER COMPILER",
		"source: THIRD PARTY",
		"source: APPLICATION",
		"source: OTHER"
	};

	const char* typeStrings[] = {
		"type: ERROR",
		"type: DEPRECATED BEHAVIOUR",
		"type: UNDEFINED BEHAVIOUR",
		"type: PORTABILITY",
		"type: PERFORMANCE",
		"type: OTHER"
	};

	int sourceId = source - 0x8246;
	int typeId = type - 0x824C;
	int severityId;
	if (severity != GL_DEBUG_SEVERITY_NOTIFICATION)
		severityId = severity - GL_DEBUG_SEVERITY_HIGH;
	else
	{
#if GL_DEBUG_NOTIFICATIONS
		sev_i = 3;
#else
		return;
#endif
	}

	printf("%s\n %s\n id: %u %s length: %i %s userParam: %i \n\n",
		sourceStrings[sourceId], typeStrings[typeId], id, severityStrings[severityId],
		length, message, *(int*)userParam);
}

void Game::Init()
{
	InitWindowParams();
	InitGLFW();
	InitInput();
	InitGLEW();
	InitShaderManager();
	InitRenderer();
	InitFaceCulling();
	InitDepthTest();
	InitViewport();
}

void Game::Loop()
{
	while(!glfwWindowShouldClose(m_Window.m_pHandle))
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		m_Renderer.Render(&m_ShaderManager);

		glfwSwapBuffers(m_Window.m_pHandle);
		glfwPollEvents();
	}
}

void Game::Terminate()
{
	glfwDestroyWindow(m_Window.m_pHandle);
	glfwTerminate();
}

void Game::InitRenderer()
{
	m_Renderer.Init();
}

void Game::InitShaderManager()
{
	m_ShaderManager.Init();
}

void Game::InitWindowParams()
{
	m_Window.m_WindowWidth = 800;
	m_Window.m_WindowHeight = 600;
}

void Game::InitGLFW()
{
	const char* title = "Nightmare Creatures Reloaded";

	glfwSetErrorCallback(GlfwErrorCallback);

	// GLFW Init
	if (!glfwInit())
	{
		printf("Failed to initialize GLFW");
		exit(1);
	}
	glfwWindowHint(GLFW_SAMPLES, GLFW_DONT_CARE); // change to 16 for good looks
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#ifdef _DEBUG
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
	glfwWindowHint(GLFW_SRGB_CAPABLE, GL_TRUE);
	m_Window.m_pHandle = glfwCreateWindow(m_Window.m_WindowWidth, m_Window.m_WindowHeight, title, nullptr, nullptr);
	if (m_Window.m_pHandle == nullptr)
	{
		printf("Failed to create GLFW window\n");
		glfwTerminate();
		exit(2);
	}
	glEnable(GL_MULTISAMPLE);
	glfwMakeContextCurrent(m_Window.m_pHandle);

	glfwSetInputMode(m_Window.m_pHandle, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
}

void Game::InitGLEW()
{
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		printf("Failed to initialize GLEW\n");
		Terminate();
		exit(3);
	}
#if _DEBUG
	if (GLEW_KHR_debug) {
		int param = -1;
		glDebugMessageCallback(KhrErrorCallback, &param);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		printf("KHR debug callback engaged\n");
	}
	else {
		printf("KHR_debug extension NOT found\n");
	}
#endif
}

void Game::InitInput()
{
	m_InputHandler.Init(m_Window.m_pHandle);
}

void Game::InitFaceCulling()
{
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
}

void Game::InitDepthTest()
{
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glDepthRange(0.0f, 1.0f);
	glEnable(GL_DEPTH_CLAMP);
}

void Game::InitViewport()
{
	glViewport(0, 0, m_Window.m_WindowWidth, m_Window.m_WindowHeight);
}
