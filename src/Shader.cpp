#include "Shader.h"
#include <glm/gtc/type_ptr.hpp>

void Shader::Use() const
{
	glUseProgram(handle);
}

void Shader::SetUniform(const char* name, float x, float y, float z) const
{
	GLint location = glGetUniformLocation(handle, name);
	glUniform3f(location, x, y, z);
}

void Shader::SetUniform(const char* name, const glm::vec3& vector) const
{
	GLint location = glGetUniformLocation(handle, name);
	glUniform1fv(location, 3, glm::value_ptr(vector));
}

void Shader::SetUniform(const char* name, const glm::vec4& vector) const
{
	GLint location = glGetUniformLocation(handle, name);
	glUniform1fv(location, 4, glm::value_ptr(vector));
}

void Shader::SetUniform(const char* name, const glm::mat4& matrix) const
{
	GLint location = glGetUniformLocation(handle, name);
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));

}

void Shader::SetUniform(const char* name, const glm::mat3& matrix) const
{
	GLint location = glGetUniformLocation(handle, name);
	glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::SetUniform(const char* name, float value) const
{
	GLint location = glGetUniformLocation(handle, name);
	glUniform1f(location, value);
}

void Shader::SetUniform(const char* name, int value) const
{
	GLint location = glGetUniformLocation(handle, name);
	glUniform1i(location, value);
}

void Shader::SetUniform(const char* name, bool value) const
{
	GLint location = glGetUniformLocation(handle, name);
	glUniform1i(location, value);
}

void Shader::AddUniformLocation(const char* name)
{
	GLint location = glGetUniformLocation(handle, name);
	uniformList.emplace(name, location);
}

void Shader::Terminate()
{
	glUseProgram(NULL);
}
