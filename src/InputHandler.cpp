#include <algorithm>
#include <GLFW/glfw3.h>
#include "InputHandler.h"

void InputHandler::Init(GLFWwindow* window)
{
	glfwSetWindowUserPointer(window, this);
	glfwSetKeyCallback(window, InputHandler::OnKeyPress);
}

bool InputHandler::Register(KeyListener* listener)
{
	listeners.push_back(listener);
	return true;
}

bool InputHandler::Unregister(KeyListener* listener)
{
	auto pos = std::find(listeners.begin(), listeners.end(), listener);
	if (pos == listeners.end())
		return false;
	listeners.erase(pos);
	return true;
}

void InputHandler::OnKeyPress(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	InputHandler* inputHandler = reinterpret_cast<InputHandler*>(glfwGetWindowUserPointer(window));
	for (KeyListener* listener : inputHandler->listeners)
		listener->OnKeyPress(key, action, mods);
}
