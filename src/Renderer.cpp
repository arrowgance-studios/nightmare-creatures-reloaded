#include "Renderer.h"
#include "ShaderManager.h"
#include "TextureManager.h"

void Renderer::Init()
{
	CreateTriangleVbo();
	CreateTriangleVao();
	CreateTriangleTextrue();
}

void Renderer::Render(ShaderManager* shaderManager)
{
	Shader shader = shaderManager->GetShader(ShaderType::TRIANGLE);
	shader.Use();
	{
		shader.SetUniform("diffuseTexture", 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_TriangleTexture);
		glBindVertexArray(m_TriangleVao);
		glDrawArrays(GL_TRIANGLES, 0, 3);
	}
	shader.Terminate();
}

void Renderer::CreateTriangleVbo()
{
	constexpr float vertexData[] = {
		// position coordinates
		-0.75f, -0.75f, 0.0f, 1.0f,
		0.75f, -0.75f, 0.0f, 1.0f,
		0.75f, 0.75f, 0.0f, 1.0f,
		// texture coordinates
		0.f, 0.f,
		1.f, 0.f,
		1.f, 1.f
	};

	glGenBuffers(1, &m_TriangleVbo);

	glBindBuffer(GL_ARRAY_BUFFER, m_TriangleVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Renderer::CreateTriangleVao()
{
	glGenVertexArrays(1, &m_TriangleVao);
	glBindVertexArray(m_TriangleVao);
	glBindBuffer(GL_ARRAY_BUFFER, m_TriangleVbo);

	constexpr unsigned int positionAttributeIndex = 0;
	constexpr unsigned int textureAttributeIndex = 1;

	glEnableVertexAttribArray(positionAttributeIndex);
	glEnableVertexAttribArray(textureAttributeIndex);
	glVertexAttribPointer(positionAttributeIndex, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
	glVertexAttribPointer(textureAttributeIndex, 2, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(float) * 12));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Renderer::CreateTriangleTextrue()
{
	TextureManager tempTextureManager;
	GLuint textureHandle = tempTextureManager.Get("texture.jpg");
	m_TriangleTexture = textureHandle;
}
