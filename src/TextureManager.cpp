#include "TextureManager.h"

#define STBI_ONLY_JPEG
#define STBI_ONLY_PNG
#define STBI_ONLY_TGA
#define STB_IMAGE_IMPLEMENTATION

#include <stb_image/stb_image.h>
#include <cstdio>

GLuint TextureManager::Get(const std::string& path)
{
	if (m_TextureMap.count(path) == 0)
	{
		std::string completePath = "../assets/textures/" + path;

		int width;
		int height;
		int numberOfComponents;
		unsigned char* image = stbi_load(completePath.c_str(), &width, &height, &numberOfComponents, STBI_rgb_alpha);

		GLuint textureHandle;
		if (image == nullptr)
		{
			fprintf(stderr, "Invalid path to file");
			return 0;
		}

		glGenTextures(1, &textureHandle);

		glBindTexture(GL_TEXTURE_2D, textureHandle);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, NULL);

		stbi_image_free(image);

		m_TextureMap.emplace(path, textureHandle);
	}

	return m_TextureMap[path];
}

bool TextureManager::Dispose(const std::string& path)
{
	if (m_TextureMap.count(path) == 0)
		return false;

	GLuint textureHandle = m_TextureMap.at(path);

	glDeleteTextures(1, &textureHandle);
	m_TextureMap.erase(path);
	return true;
}

