#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "ShaderManager.h"

static std::string FileToString(const std::string& path)
{
	std::string shadersDirectory = "../assets/shaders/";
	std::ifstream fileStream(shadersDirectory + path);
	std::stringstream buffer;
	buffer << fileStream.rdbuf();

	if (buffer.fail())
		std::cerr << "File " << path << " not found" << std::endl;

	return buffer.str();
}

void ShaderManager::Init()
{
	//InitBasicShader();
	InitTriangleShader();

}

Shader ShaderManager::GetShader(ShaderType type)
{
	unsigned int index = (unsigned int)type;
	return m_shaders[index];
}

// Author: Jason L. McKesson

GLuint ShaderManager::CreateProgram(const std::string& vertexShaderPath, const std::string& fragmentShaderPath)
{
	std::vector<GLuint> shaders = {
		CreateShader(GL_VERTEX_SHADER, vertexShaderPath),
		CreateShader(GL_FRAGMENT_SHADER, fragmentShaderPath)
	};

	GLuint program = glCreateProgram();

	for (size_t iLoop = 0; iLoop < shaders.size(); iLoop++)
		glAttachShader(program, shaders[iLoop]);

	glLinkProgram(program);

	GLint status;
	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		GLint infoLogLength;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar *strInfoLog = new GLchar[infoLogLength + 1];
		glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
		fprintf(stderr, "Linker failure: %s\n", strInfoLog);
		delete[] strInfoLog;
	}

	for (size_t iLoop = 0; iLoop < shaders.size(); iLoop++)
		glDetachShader(program, shaders[iLoop]);

	for (size_t iLoop = 0; iLoop < shaders.size(); iLoop++)
		glDeleteShader(shaders[iLoop]);

	return program;
}

// Author: Jason L. McKesson

GLuint ShaderManager::CreateShader(GLenum type, const std::string& fileName)
{
	std::string fileData = FileToString(fileName);

	GLuint shader = glCreateShader(type);
	const char *strFileData = fileData.c_str();
	glShaderSource(shader, 1, &strFileData, NULL);

	glCompileShader(shader);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		GLint infoLogLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar *strInfoLog = new GLchar[infoLogLength + 1];
		glGetShaderInfoLog(shader, infoLogLength, NULL, strInfoLog);

		const char *strShaderType = NULL;
		switch (type)
		{
		case GL_VERTEX_SHADER: strShaderType = "vertex"; break;
		case GL_GEOMETRY_SHADER: strShaderType = "geometry"; break;
		case GL_FRAGMENT_SHADER: strShaderType = "fragment"; break;
		}

		fprintf(stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog);
		delete[] strInfoLog;
		throw std::runtime_error("Shader linking error");
	}

	return shader;
}

void ShaderManager::InitBasicShader()
{
	GLuint handle = CreateProgram("todo", "todo");
	Shader shader;
	shader.handle = handle;
	m_shaders[(unsigned int)ShaderType::BASIC] = shader;
}

void ShaderManager::InitTriangleShader()
{
	GLuint handle = CreateProgram("Triangle.vert", "Triangle.frag");
	Shader shader;
	shader.handle = handle;
	m_shaders[(unsigned int)ShaderType::TRIANGLE] = shader;

	shader.Use();
	{
		shader.AddUniformLocation("diffuseTexture");
		shader.SetUniform("diffuseTexture", 0);
	}
	shader.Terminate();
}
