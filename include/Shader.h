#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>
#include <string>
#include <map>

enum class ShaderType
{
	BASIC,
	TRIANGLE,
	COUNT
};

struct Shader
{
	void Use() const;
	void SetUniform(const char* name, float x, float y, float z) const;
	void SetUniform(const char* name, const glm::vec3& vector) const;
	void SetUniform(const char* name, const glm::vec4& vector) const;
	void SetUniform(const char* name, const glm::mat4& matrix) const;
	void SetUniform(const char* name, const glm::mat3& matrix) const;
	void SetUniform(const char* name, float value) const;
	void SetUniform(const char* name, int value) const;
	void SetUniform(const char* name, bool value) const;
	void AddUniformLocation(const char* name);

	static void Terminate();

public:
	GLuint handle;
	std::map<std::string, GLuint> uniformList;
};