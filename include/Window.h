#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

#include <GLFW/glfw3.h>

struct Window
{
	GLFWwindow* m_pHandle;
	int			m_WindowWidth;
	int			m_WindowHeight;
};
