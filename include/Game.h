#pragma once
#include "Window.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "InputHandler.h"
#include "World.h"
#include "ShaderManager.h"
#include "TextureManager.h"

class Game
{
public:
	void Init();
	void Loop();
	void Terminate();

private:
	void InitRenderer();
	void InitShaderManager();
	void InitWindowParams();
	void InitGLFW();
	void InitGLEW();
	void InitInput();
	void InitFaceCulling();
	void InitDepthTest();
	void InitViewport();

private:
	Window m_Window;
	ResourceManager m_ResourceMgr;
	ShaderManager m_ShaderManager;
	Renderer m_Renderer;
	InputHandler m_InputHandler;
	TextureManager m_TextureManager;
	World m_World;
};
