#pragma once

#include <deque>
#include <GLFW/glfw3.h>
#include "KeyListener.h"

class InputHandler
{
public:
	void Init(GLFWwindow* window);
	bool Register(KeyListener* listener);
	bool Unregister(KeyListener* listener);
	static void OnKeyPress(GLFWwindow* window, int key, int scancode, int action, int mods);

private:
	std::deque<KeyListener*> listeners;
};
