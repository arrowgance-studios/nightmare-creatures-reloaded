#pragma once
#include <string>
#include <unordered_map>

#define GLEW_STATIC
#include <GL/glew.h>

class TextureManager
{
public:
	GLuint Get(const std::string& path);
	bool Dispose(const std::string& path);
private:
	std::unordered_map<std::string, GLuint> m_TextureMap;
};
