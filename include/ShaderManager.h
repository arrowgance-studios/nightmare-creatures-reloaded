#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

#include <string>
#include "Shader.h"

constexpr unsigned int MAX_SHADERS = (unsigned int)ShaderType::COUNT;

class ShaderManager
{
public:
	void Init();
	Shader GetShader(ShaderType type);

private:
	GLuint CreateProgram(const std::string& vertexShaderRaw, const std::string& fragmentShaderRaw);
	GLuint CreateShader(GLenum type, const std::string& fileName);
	void InitBasicShader();
	void InitTriangleShader();

	Shader m_shaders[MAX_SHADERS];
};

