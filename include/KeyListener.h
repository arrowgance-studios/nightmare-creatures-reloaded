#pragma once

class KeyListener
{
public:
	virtual bool OnKeyPress(int key, int action, int mods) = 0;
};
