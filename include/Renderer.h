#pragma once
#define GLEW_STATIC
#include <GL/glew.h>

class ShaderManager;

class Renderer
{
public:
	void Init();
	void Render(ShaderManager*);

private:
	void CreateTriangleVbo();
	void CreateTriangleVao();
	void CreateTriangleTextrue();

private:
	GLuint m_TriangleVao;
	GLuint m_TriangleVbo;
	GLuint m_TriangleTexture;
};
